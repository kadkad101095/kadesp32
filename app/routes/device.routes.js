module.exports = (app) => {
    const device = require('../controllers/device.controller.js');

    app.get('/device/getAll', device.getAll);
    app.post('/device/addnew', device.create);
    app.post('/device/update', device.update);
};