module.exports = (app) => {
    const admin = require('../controllers/admin.controller.js');

    // Create a default Admin
    app.all('/generateadmin/default', admin.create);
    // Create new Admin
    app.all('/adminCreate', admin.createNew);
    //Login
    app.all('/admin/login/:username/:password', admin.login);

};