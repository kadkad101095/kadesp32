module.exports = (app) => {
    const uploader = require('../controllers/uploader.controller.js');

    app.get('/file/getAll', uploader.getAll);
    app.post('/file/uploadBase64', uploader.uploadBase64);
    app.post('/file/upload', uploader.upload().single('file'), (req, res, next) => {
        res.json(uploader.success(req));
    });

};
