const mongoose = require('mongoose');

const Admin = mongoose.model('Admin', {
    username: {type: String, required: true},
    encryptedPassword: {type: String, required: true},
    role: {type: String, enum: ['admin', 'restricted'], required: true}
});

module.exports = Admin;