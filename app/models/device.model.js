const mongoose = require('mongoose');

const Device = mongoose.model('Device', {
    name: {type: String, required: true},
    state: {type: String, enum: ['ON', 'OFF'], required: true},
    port: {type: String, required: true}
});

module.exports = Device;