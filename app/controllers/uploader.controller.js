const handler = require('../utils/ResponseHandler');
const multer = require('multer');
const fs = require('fs');
const folder = './assets/images/';

var uploader = {

    storage: function () {
        return multer.diskStorage({
            destination: (req, file, cb) => {
                var path = folder;
                fs.mkdirSync(path, {recursive: true});
                cb(null, path);
            },
            filename: (req, file, cb) => {
                console.log(file);
                var filetype = '';
                if (file.mimetype === 'image/gif') {
                    filetype = 'gif';
                }
                if (file.mimetype === 'image/png') {
                    filetype = 'png';
                }
                if (file.mimetype === 'image/jpeg') {
                    filetype = 'jpg';
                }
                if (file.mimetype === 'image/jpg') {
                    filetype = 'jpg';
                }
                cb(null, file.originalname + '.' + filetype);
            }
        });
    },

    upload: function () {
        return multer({storage: uploader.storage()});
    },

    success: function (req) {
        return handler.success('Upload success');
    },

    getAll: function (req, res) {
        var arr = [];
        fs.readdirSync(folder).forEach(file => {
            if (file !== '.DS_Store') {
                arr.push(file);
            }
        });

        res.send(handler.success(arr));
    },

    uploadBase64: function (req, res) {
        let name = new Date().getTime();
        let content = req.body.content;
        if (!content) {
            res.json(handler.lackInformation());
            return;
        }
        fs.writeFile(folder + name + ".jpeg", content, 'base64', function (err) {
            if (err) {
                res.send(handler.failed(err));
            } else {
                res.send(handler.success());
            }
        });
    }

};

module.exports = uploader;