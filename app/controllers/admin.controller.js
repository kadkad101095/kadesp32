const Admin = require('../models/admin.model.js');
const bcrypt = require('bcrypt');
const handler = require('../utils/ResponseHandler');

// Create and Save a new Note
exports.create = (req, res) => {
    Admin.find()
            .then(admins => {
                if (admins && admins.length > 0) {
                    res.status(201).send({
                        message: "Already have admin"
                    });
                } else {
                    // Create a Admin
                    bcrypt.hash('admin', 8, function (err, hash) {
                        const admin = new Admin({
                            username: "admin",
                            encryptedPassword: hash,
                            role: "admin"
                        });

                        // Save Note in the database
                        admin.save()
                                .then(data => {
                                    res.send(data);
                                }).catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred while creating the Note."
                            });
                        });
                    });
                }
            }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.createNew = (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    if (!username || !password) {
        res.json(handler.lackInformation());
        return;
    }
    Admin.find({username: username})
            .then(admins => {
                if (admins && admins.length > 0) {
                    res.json(handler.failed("Tài khoản đã tồn tại"));
                } else {
                    // Create a Admin
                    bcrypt.hash(password, 8, function (err, hash) {
                        const admin = new Admin({
                            username: username,
                            encryptedPassword: hash,
                            role: "restricted"
                        });

                        // Save Note in the database
                        admin.save()
                                .then(data => {
                                    res.send(handler.success(data));
                                }).catch(err => {
                            res.json(handler.failed(err.message || "Some error occurred while creating the Note."));
                        });
                    });
                }
            }).catch(err => {
        res.json(handler.failed(err.message || "Some error occurred while creating the Note."));
    });
};

exports.login = (req, res) => {
    let username = req.params.username;
    let password = req.params.password;
    authenticate(username, password, (result) => {
        if (result === 0) {
            res.json(handler.success());
            return;
        }
        if (result === 1) {
            res.json(handler.failed("Wrong password"));
            return;
        }
        if (result === 2) {
            res.json(handler.failed("Wrong username"));
            return;
        }
        res.json(handler.failed("I don't know"));
    });
};

async function authenticate(username, password, callback) {
    const user = await Admin.findOne({username});
    if (user) {
        const matched = await bcrypt.compare(password, user.encryptedPassword);
        if (matched) {
            callback(0); //Ok
        } else {
            callback(1); //Wrong password
        }
    } else {
        callback(2); //Wrong username
    }
}