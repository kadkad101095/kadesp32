const Device = require('../models/device.model.js');
const handler = require('../utils/ResponseHandler');
const fetch = require('node-fetch');

exports.getAll = (req, res) => {
    Device.find().then(news => {
        res.json(handler.success(news));
    });
};

exports.create = (req, res) => {
    let state = req.body.state;
    let name = req.body.name;
    let port = req.body.port;

    if (!state || !name) {
        res.json(handler.lackInformation());
        return;
    }

    let item = new Device({
        deviceName: name,
        state: state,
        port: port
    });
    // Save Note in the database
    item.save().then(data => {
        res.send(handler.success(data));
    }).catch(err => {
        res.send(handler.failed(err));
    });
};

exports.update = (req, res) => {
    let id = req.body.id;
    let name = req.body.name;
    let state = req.body.state;
    let port = req.body.port;

    if (!id || !name || !state || !port) {
        res.json(handler.lackInformation());
        return;
    }

    Device.findById(id, function (err, item) {
        if (!item || err) {
            res.json(handler.failed("Failed to find this device"));
        } else {
            item.name = name;
            item.state = state;
            item.port = port;
            item.save().then(data => {
                res.send(handler.success(data));
            }).catch(err => {
                res.send(handler.failed(err));
            });
        }
    });
};