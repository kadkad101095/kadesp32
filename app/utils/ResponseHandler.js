
var responseHandler = {
    success: function (data) {
        return myResponse = {
            'code': 200,
            'message': '',
            'data': data
        };
    },
    failed: function (message) {
        return {
            'code': 201,
            'message': message,
            'data': null
        };
    },
    lackInformation() {
        return {
            'code': 202,
            'message': 'Please fill all information',
            'data': null
        };
    },
    emailExisted() {
        return {
            'code': 203,
            'message': 'Email is existed',
            'data': null
        };
    },
    incorrectEmail() {
        return {
            'code': 204,
            'message': 'Incorrect email',
            'data': null
        };
    },
    incorrectPassword() {
        return {
            'code': 205,
            'message': 'Incorrect password',
            'data': null
        };
    },
    invalidPagingParam() {
        return {
            'code': 206,
            'message': 'Invalid paging param, `from` must lower than `to`',
            'data': null
        };
    },
    userHaveNoImage() {
        return {
            'code': 207,
            'message': 'User have no image. Please upload body image',
            'data': null
        };
    },
    userImageProcessing() {
        return {
            'code': 208,
            'message': 'User image is processing, please wait for notification',
            'data': null
        };
    },
    emailNotExist() {
        return {
            'code': 209,
            'message': 'Email is not existed',
            'data': null
        };
    },
    invalidToken() {
        return {
            'code': 210,
            'message': 'Invalid token',
            'data': null
        };
    },
    invalidShopId() {
        return {
            'code': 211,
            'message': 'Invalid shop id',
            'data': null
        };
    },
    invalidGoogleToken() {
        return {
            'code': 212,
            'message': 'Invalid google token',
            'data': null
        };
    },
    invalidFacebookToken() {
        return {
            'code': 213,
            'message': 'Invalid facebook token',
            'data': null
        };
    },
    facebookNoEmail() {
        return {
            'code': 214,
            'message': 'This account have no email',
            'data': null
        };
    }
};

module.exports = responseHandler;