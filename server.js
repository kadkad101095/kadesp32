const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 7004;
const assetRoot = __dirname;
const pageRoot = __dirname + '/assets/images';
const AdminBro = require('admin-bro');
const AdminBroMongoose = require('@admin-bro/mongoose');
const AdminBroExpress = require('@admin-bro/express');
const bcrypt = require('bcrypt');
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');
const mycookiePassword = "KAD-ESP32-PASSWORD";
AdminBro.registerAdapter(AdminBroMongoose);
const server = require('http').createServer(app);

//Resources definitions
const Admin = require('./app/models/admin.model.js');
const Device = require('./app/models/device.model.js');

const adminBro = new AdminBro({
    resources: [Admin, Device],
    rootPath: '/admin'
});

// Build and use a router which will handle all AdminBro routes
const router = AdminBroExpress.buildAuthenticatedRouter(adminBro, {
    authenticate: async (username, password) => {
        const user = await Admin.findOne({username});
        if (user) {
            const matched = await bcrypt.compare(password, user.encryptedPassword);
            if (matched) {
                return user;
            }
        }
        return false;
    },
    cookiePassword: mycookiePassword
});

app.use(adminBro.options.rootPath, router);
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(express.static(assetRoot));
app.use(express.static(pageRoot));
app.set('views', pageRoot);
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Routes
require('./app/routes/admin.routes.js')(app);
require('./app/routes/device.routes.js')(app);
require('./app/routes/uploader.routes.js')(app);

const run = async () => {
    await mongoose.connect(dbConfig.url, {useNewUrlParser: true, useUnifiedTopology: true});
    await server.listen(port, () => console.log('Dat ESP32 server started on: ' + port));
};

run();